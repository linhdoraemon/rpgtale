package com.wingx.pubmine.boss;

import org.bukkit.entity.ArmorStand;

public class ArmorstandBoss extends Boss {

	private ArmorStand owner;
	
	public ArmorstandBoss(ArmorStand owner) {
		super(owner);
		
		this.owner = owner;
	}

	public ArmorStand getOwner() {
		return owner;
	}

	public void update() {
		owner.setBasePlate(false);
		owner.setArms(true);
	}
	
	
}
