package com.wingx.pubmine.boss;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.KeyedBossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.wingx.pubmine.boss.skill.BossSkills;
import com.wingx.pubmine.util.Nearby;

public class Boss {

	private String displayname;
	private int Totalhealth;
	private KeyedBossBar bar;
	private String death_message;
	private BossSkills[] skills;
	private double skill_radius;
	private LivingEntity owner;

	public Boss(LivingEntity owner) {
		this.owner = owner;
		this.bar = Bukkit.createBossBar(new NamespacedKey(Bukkit.getPluginManager().getPlugin("RPGTale"), displayname),
				displayname + " §f[§c" + (int) owner.getHealth() + "§7/§e" + owner.getHealth() + "§f]", BarColor.RED,
				BarStyle.SEGMENTED_6, BarFlag.PLAY_BOSS_MUSIC);
	}
	
	public String getDisplayName() {
		return displayname;
	}

	public void setDisplayName(String dis) {
		this.displayname = dis;
		owner.setCustomName(dis);
		owner.setCustomNameVisible(true);
	}
	
	public KeyedBossBar getHealthBar() {
		return bar;
	}

	public LivingEntity getOwner() {
		return owner;
	}

	public int getTotalHealth() {
		return Totalhealth;
	}
	
	public void setTotalHealth(int health) {
		this.Totalhealth = health;
		owner.setMaxHealth(health);
	}
	
	public String getDeathMessenger() {
		return death_message;
	}

	public void setDeathMessage(String msg) {
		this.death_message = msg;
	}

	public void setItemInMainHand(ItemStack item, float drop_chance) {
		owner.getEquipment().setItemInMainHand(item);
		owner.getEquipment().setItemInMainHandDropChance(drop_chance);
	}
	
	public void setItemInOffHand(ItemStack item, float drop_chance) {
		owner.getEquipment().setItemInOffHand(item);
		owner.getEquipment().setItemInOffHandDropChance(drop_chance);
	}

	public void setHelmet(ItemStack item, float drop_chance) {
		owner.getEquipment().setHelmet(item);
		owner.getEquipment().setHelmetDropChance(drop_chance);
	}
	
	public void setChestplate(ItemStack item, float drop_chance) {
		owner.getEquipment().setChestplate(item);
		owner.getEquipment().setChestplateDropChance(drop_chance);
	}
	
	public void setLeggings(ItemStack item, float drop_chance) {
		owner.getEquipment().setLeggings(item);
		owner.getEquipment().setLeggingsDropChance(drop_chance);
	}
	
	public void setBoots(ItemStack item, float drop_chance) {
		owner.getEquipment().setBoots(item);
		owner.getEquipment().setBootsDropChance(drop_chance);
	}
	
	public void setSkills(BossSkills... bossSkills) {
		this.skills = bossSkills;
	}

	public List<Player> getEnemyNear() {
		List<Player> list = new ArrayList();
		for (Entity en : Nearby.getEntitiesAroundPoint(owner.getLocation(), getSkillRange())) {
			if (en instanceof Player) {
				list.add((Player) en);
			}
		}
		return list;
	}

	public void setSkillRange(double radius) {
		this.skill_radius = radius;
	}

	public double getSkillRange() {
		return skill_radius;
	}

	public BossSkills[] getSkills() {
		return skills;
	}

	public void startSkilling() {
		new StartSkilling(this).runTaskTimer(Bukkit.getPluginManager().getPlugin("RPGTale"), 5*20, 5*20);
	}

	public void updateHealthBar() {
		double current_health = owner.getHealth();
		double max_health = getTotalHealth();
		
		double percent = ((current_health / max_health));
		double progress = Math.floor((percent*100) / 100d);
	
		getHealthBar().setTitle(displayname + " §f[§c" + (int) owner.getHealth() + "§7/§e" + getTotalHealth() + "§f]");
		getHealthBar().setProgress(progress);
		
		for(Player p : getEnemyNear()) {
			if(getHealthBar().getPlayers().contains(p)) {
				return;
			}
			
			getHealthBar().addPlayer(p);
		}
	}
	
	public boolean isDead() {
		return owner.isDead();
	}
	
	class StartSkilling extends BukkitRunnable {

		private Boss owner;
		
		public StartSkilling(Boss owner) {
			this.owner = owner;
		}
		
		@Override
		public void run() {
			if(owner.isDead()) {
				cancel();
			}
			
			BossSkills skill = Arrays.asList(getSkills()).get(getSkills().length);
			skill.activate();
		}
		
	}

	
	
}
