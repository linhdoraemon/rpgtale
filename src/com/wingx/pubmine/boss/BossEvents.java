package com.wingx.pubmine.boss;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import com.wingx.pubmine.util.HUDUtils;

public class BossEvents implements Listener {

	@EventHandler
	public void BOSS_GET_DAMAGE_CALL_EVENT(EntityDamageEvent e) {
		Entity en = e.getEntity();

		if (en == null) {
			return;
		}

		if (!(en instanceof Boss)) {
			return;
		}

		Boss boss = (Boss) en;
		boss.updateHealthBar();
	}

	@EventHandler
	public void BOSS_DEATH_CALL_EVENT(EntityDeathEvent e) {
		Entity en = e.getEntity();

		if (en == null) {
			return;
		}

		if (!(en instanceof Boss)) {
			return;
		}

		Boss boss = (Boss) en;

		if (e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) e.getEntity().getLastDamageCause();
			Entity damager = damageEvent.getDamager();

			if (damager != null) {
				if (damager instanceof Projectile) {
					Projectile projectile = (Projectile) damager;
					if (projectile.getShooter() != null && projectile.getShooter() instanceof Player) {
						HUDUtils.sendAllMessage(boss.getDeathMessenger().replaceAll("%s",
								((Player) projectile.getShooter()).getName()));
						((Player) projectile.getShooter()).sendTitle(boss.getDisplayName(),
								"§a§lCHÚC MỪNG BẠN ĐÃ TIÊU DIỆT");
					}
				} else if (damager instanceof TNTPrimed) {
					TNTPrimed tnt = (TNTPrimed) damager;
					if (tnt.getSource() instanceof Player) {
						Player source = (Player) tnt.getSource();
						if (source != null && source.isValid()) {
							HUDUtils.sendAllMessage(
									boss.getDeathMessenger().replaceAll("%s", ((Player) source).getName()));
							((Player) source).sendTitle(boss.getDisplayName(), "§a§lCHÚC MỪNG BẠN ĐÃ TIÊU DIỆT");
						}
					}
				} else {
					if (damager instanceof Player) {
						HUDUtils.sendAllMessage(
								boss.getDeathMessenger().replaceAll("%s", ((Player) damager).getName()));
						((Player) damager).sendTitle(boss.getDisplayName(), "§a§lCHÚC MỪNG BẠN ĐÃ TIÊU DIỆT");
					} else {
						HUDUtils.sendAllMessage(boss.getDeathMessenger().replaceAll("%s", "Unknown"));
					}
				}
			}
		}
	}

}
