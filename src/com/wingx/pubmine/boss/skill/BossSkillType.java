package com.wingx.pubmine.boss.skill;

public enum BossSkillType {

	POTION,
	THUNDER,
	KNOCKUP,
	REPULSE,
	FIRE,
	TELEPORT_SELF,
	TELEPORT_ENEMY,
	INVISIBILITY_SELF,
	INVISIBILITY_ENEMY;

}
