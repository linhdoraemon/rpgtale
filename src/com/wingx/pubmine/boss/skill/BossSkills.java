package com.wingx.pubmine.boss.skill;

import java.util.Random;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.wingx.pubmine.boss.Boss;

public class BossSkills {

	private double damage;
	private BossSkillType type;
	private Boss owner;
	private Sound sound;
	private PotionEffectType potion_type;
	private String activate_msg;
	private BossSideSkills side_skill;

	public BossSkills(Boss owner, BossSkillType type, double damage) {
		this.damage = damage;
		this.type = type;
		this.owner = owner;
	}

	public void setDamage(double dmg) {
		this.damage = dmg;
	}

	public double getDamage() {
		return damage;
	}

	public void setSideSkills(BossSideSkills skill) {
		this.side_skill = skill;
	}
	
	public BossSideSkills getSideSkills() {
		return side_skill;
	}
	
	public BossSkillType getSkillType() {
		return type;
	}

	public void setSound(Sound sound) {
		this.sound = sound;
	}

	public Sound getSound() {
		return sound;
	}

	public Boss getOwner() {
		return owner;
	}

	public void setPotionSkillType(PotionEffectType type) {
		this.potion_type = type;
	}

	public PotionEffectType getPotionSkillType() {
		return this.potion_type;
	}

	public void setActivateMessage(String msg) {
		this.activate_msg = msg;
	}

	public String getActivateMessage() {
		return this.activate_msg;
	}

	public void activate() {
		switch (type) {
		case POTION:
			for (Player enemy : owner.getEnemyNear()) {
				enemy.addPotionEffect(new PotionEffect(getPotionSkillType(), 20 * 3, 2));
				activateAction(enemy);
			}
			activateSideSkill();
			break;

		case FIRE:
			for (Player enemy : owner.getEnemyNear()) {
				enemy.setFireTicks(20 * 5);
				activateAction(enemy);
			}
			activateSideSkill();
			break;
		case INVISIBILITY_SELF:
			owner.getOwner().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 3 * 20, 2));
			for (Player enemy : owner.getEnemyNear()) {
				activateAction(enemy);
			}
			activateSideSkill();
			break;
		case INVISIBILITY_ENEMY:
			for (Player enemy : owner.getEnemyNear()) {
				enemy.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 3 * 20, 2));
				activateAction(enemy);
			}
			activateSideSkill();
			break;
		case KNOCKUP:
			for (Player enemy : owner.getEnemyNear()) {
				enemy.setVelocity(new Vector(0.0F, 8F, 0.0F));
				activateAction(enemy);
			}
			activateSideSkill();
			break;
		case REPULSE:
			for (Player en : owner.getEnemyNear()) {
				final Vector velocity = en.getLocation().subtract(owner.getOwner().getLocation()).toVector();
				velocity.setY(velocity.getY() / 3);
				en.setVelocity(velocity.multiply(8 / (1 + velocity.lengthSquared())));
				activateAction(en);
			}
			activateSideSkill();
			break;
		case TELEPORT_ENEMY:
			Player p = owner.getEnemyNear().get(new Random().nextInt(owner.getEnemyNear().size()));
			p.teleport(owner.getOwner());
			p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 20, 1);
			for (Player pl : owner.getEnemyNear()) {
				activateAction(pl);
			}
			activateSideSkill();
			break;
		case TELEPORT_SELF:
			Player player = owner.getEnemyNear().get(new Random().nextInt(owner.getEnemyNear().size()));
			owner.getOwner().teleport(player);
			player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 20, 1);
			for (Player pl : owner.getEnemyNear()) {
				activateAction(pl);
			}
			activateSideSkill();
			break;
		default:
			for(Player enemy : owner.getEnemyNear()) {
				enemy.getWorld().strikeLightningEffect(enemy.getLocation());
				enemy.setHealth(enemy.getHealth() - getDamage());
				activateAction(enemy);
			}
			activateSideSkill();
			break;
		}
	}

	private void activateAction(Player enemy) {
		enemy.sendMessage(getActivateMessage());
		enemy.playSound(enemy.getLocation(), getSound(), 1, 1);
	}

	private void activateSideSkill() {
		if(side_skill == null) {
			return;
		}
		
		getSideSkills().activate();
	}
	
}
