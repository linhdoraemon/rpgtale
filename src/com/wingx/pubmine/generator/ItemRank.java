package com.wingx.pubmine.generator;

public enum ItemRank {

	UNCOMMON("§8§lUncommon Item"),
	COMMON("§7§lCommon Item"),
	RARE("§9§lRare Item"),
	MYTHIC("§5§lMythic Item"),
	LEGENDARY("§6§lLegendary Item");
	
	private String lore;
	
	ItemRank(String lore){
		this.lore = lore;
	}
	
	public String getLore() {
		return lore;
	}
	
}
