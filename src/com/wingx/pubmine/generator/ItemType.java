package com.wingx.pubmine.generator;

import org.bukkit.Material;

public enum ItemType {

	//
	SWORD("§3§lKiếm", new Material[] { Material.WOODEN_SWORD, Material.STONE_SWORD, Material.IRON_SWORD,
			Material.GOLDEN_SWORD, Material.DIAMOND_SWORD }),
	//
	LONG_SWORD("§3§lKiếm dài", new Material[] { Material.IRON_SWORD, Material.GOLDEN_SWORD, Material.DIAMOND_SWORD }),
	//
	GREAT_SWORD("§3§lĐoản kiếm", new Material[] { Material.IRON_SWORD, Material.DIAMOND_SWORD }),
	//
	BOW("§3§lCung tiễn", new Material[] { Material.BOW }),
	//
	AXE("§3§lRìu", new Material[] { Material.WOODEN_AXE, Material.STONE_AXE, Material.IRON_AXE, Material.GOLDEN_AXE,
			Material.DIAMOND_AXE }),
	//
	DAGGER("§3§lDao găm", new Material[] { Material.STONE_SWORD, Material.IRON_SWORD }),
	//
	ARMOR("§3§lGiáp",
			new Material[] { Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS,
					Material.LEATHER_BOOTS, Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS,
					Material.IRON_BOOTS, Material.GOLDEN_HELMET, Material.GOLDEN_CHESTPLATE, Material.GOLDEN_LEGGINGS,
					Material.GOLDEN_BOOTS, Material.DIAMOND_HELMET, Material.DIAMOND_CHESTPLATE,
					Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS }),
	//
	KATANA("§3§lKatana", new Material[] { Material.IRON_SWORD, Material.GOLDEN_SWORD, Material.DIAMOND_SWORD }),
	//
	GREAT_AXE("§3§lRìu lớn", new Material[] { Material.IRON_AXE, Material.DIAMOND_AXE }),
	//
	SHIELD("§3§lKhiên", new Material[] { Material.SHIELD }),
	//
	GREAT_SHIELD("§3§lKhiên lớn", new Material[] { Material.SHIELD }),
	//
	SPEAR("§3§lGiáo", new Material[] { Material.STICK }),
	//
	TRIDENT("§3§lĐinh ba", new Material[] { Material.TRIDENT }),
	//
	CROSSBOW("§3§lNỏ tiễn", new Material[] { Material.CROSSBOW });

	private String des;
	private Material[] attr;

	ItemType(String des, Material[] mat) {
		this.des = des;
		this.attr = mat;
	}

	ItemType(String des) {
		this.des = des;
	}

	public String getDescription() {
		return des;
	}

	public Material[] getMaterial() {
		return attr;
	}
}
