package com.wingx.pubmine.loot;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.metadata.FixedMetadataValue;

import com.wingx.pubmine.generator.ItemGenerator;
import com.wingx.pubmine.generator.ItemType;

public class LootChest {

	private int level;

	public LootChest(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
	
	public void spawnLootChest(Location loc) {
		Block b = loc.getBlock();
		b.setType(Material.CHEST);
		b.setMetadata("lootchest", new FixedMetadataValue(Bukkit.getPluginManager().getPlugin("RPGTale"), b.toString()));
		
		Chest c = (Chest) b.getState();
		Inventory i = c.getBlockInventory();
		
		int number_of_item = level*2;
		
		ItemType type = getRandomType();
		
		i.setItem(new Random().nextInt(27),
				new ItemGenerator(type, type.getMaterial()[new Random().nextInt(type.getMaterial().length)]).getRandomItem());
		
		for(int so = 1; so < number_of_item; so++) {
		}
	}

	private ItemType getRandomType() {
		return ItemType.values()[new Random().nextInt(ItemType.values().length)];
	}

}
