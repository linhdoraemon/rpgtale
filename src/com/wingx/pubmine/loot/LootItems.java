package com.wingx.pubmine.loot;

import org.bukkit.inventory.ItemStack;

import com.wingx.pubmine.element.dust.DustItem;

public class LootItems {

	public static ItemStack[] LOOT_ITEMS = { DustItem.EARTH_POWDER(1), DustItem.FIRE_POWDER(1),
			DustItem.THUNDER_POWDER(1), DustItem.WATER_POWDER(1), DustItem.WIND_POWDER(1), DustItem.EARTH_POWDER(2),
			DustItem.FIRE_POWDER(2), DustItem.THUNDER_POWDER(2), DustItem.WATER_POWDER(2), DustItem.WIND_POWDER(2),
			DustItem.EARTH_POWDER(3), DustItem.FIRE_POWDER(3), DustItem.THUNDER_POWDER(3), DustItem.WATER_POWDER(3),
			DustItem.WIND_POWDER(3), DustItem.EARTH_POWDER(4), DustItem.FIRE_POWDER(4), DustItem.THUNDER_POWDER(4),
			DustItem.WATER_POWDER(4), DustItem.WIND_POWDER(4), DustItem.EARTH_POWDER(4), DustItem.FIRE_POWDER(4),
			DustItem.THUNDER_POWDER(5), DustItem.WATER_POWDER(5), DustItem.WIND_POWDER(5) };

}
