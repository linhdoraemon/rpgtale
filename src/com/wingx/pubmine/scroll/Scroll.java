package com.wingx.pubmine.scroll;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.wingx.pubmine.generator.Enchanter;
import com.wingx.pubmine.generator.ItemRank;
import com.wingx.pubmine.generator.ItemType;
import com.wingx.pubmine.util.RomanNumerals;

public class Scroll {

	private Enchanter enchant;
	private int level;
	private int success_rate;

	public Scroll(Enchanter enchant, int level, int success_rate) {
		this.enchant = enchant;
		this.level = level;
		this.success_rate = success_rate;
	}

	public Scroll(ItemStack scroll) {
		if (isScroll(scroll) == false) {
			return;
		}

		List<String> l = scroll.getItemMeta().getLore();
		String s = l.get(1);

		for (Enchanter en : Enchanter.values()) {
			if (s.contains(en.getLore())) {
				this.enchant = en;
				break;
			}
		}

		this.success_rate = Integer.parseInt(l.get(3).split(" ")[5]);

		int level = RomanNumerals.fromNumerals(s.split(" ")[s.split(" ").length - 1]);
		this.level = level;
	}

	public int getSuccessRate() {
		return success_rate;
	}

	public int getFailureRate() {
		return 100 - success_rate;
	}

	public int getLevel() {
		return level;
	}

	public Enchanter getEnchant() {
		return enchant;
	}

	public ItemStack getItemStack() {
		ItemStack i = new ItemStack(Material.PAPER);
		ItemMeta mt = i.getItemMeta();

		int fail_rate = 100 - success_rate;

		mt.setDisplayName("§e§lCuộn giấy ma thuật");
		mt.setLore(Arrays.asList("", "§r §7" + getEnchant().getLore() + RomanNumerals.toNumerals(getLevel()), "",
				"§3Tỉ lệ thành công :§e " + success_rate, "§cTỉ lệ thất bại :§d " + fail_rate, "",
				"§6Giúp bạn nâng cấp phù phép §7" + getEnchant().getLore() + RomanNumerals.toNumerals(getLevel()),
				"§6ở vật phẩm lên §7" + getEnchant().getLore() + RomanNumerals.toNumerals(getLevel() + 1), "",
				"§a§oCầm vật phẩm cần nâng cấp trên tay phải", "§a§ovà cuộn giấy bên tay trái rồi chuyển đổi",
				"§a§ovật phẩm giữa hai tay để nâng cấp", "", ItemRank.RARE.getLore()));
		i.setItemMeta(mt);
		return i;
	}

	public static boolean isScroll(ItemStack i) {
		if (i.hasItemMeta() == false || i.getItemMeta().hasDisplayName() == false
				|| i.getItemMeta().hasLore() == false) {
			return false;
		}

		String name = i.getItemMeta().getDisplayName();

		if (name.contains("Cuộn giấy ma thuật")) {
			return true;
		}

		return false;
	}

	public static boolean upgrade(Player p, ItemStack i, ItemStack scroll) {
		if (i.hasItemMeta() == false || i.getItemMeta().hasLore() == false) {
			return false;
		}

		if (isScroll(scroll) == false) {
			p.sendMessage("§cVui lòng sử dụng đúng §e§lCuộn giấy ma thuật");
			return false;
		}

		ItemMeta mt = i.getItemMeta();
		List<String> lores = mt.getLore();
		List<String> new_lores = new ArrayList();

		Scroll sc = new Scroll(scroll);

		String string = "§r §7" + sc.getEnchant().getLore() + RomanNumerals.toNumerals(sc.getLevel());

		if(ItemType.getByLore(i).getEnchantments().contains(sc.getEnchant()) == false) {
			p.sendMessage("§cKhông thể ép phù phép §e" + sc.getEnchant().getLore() + "§cvào vật phẩm !");
			return false;
		}
		
		if (mt.hasEnchant(sc.getEnchant().getRootEnchantment()) == false) {
			if (i.getEnchantments().keySet().size() == 7) {
				p.sendMessage("§cVật phẩm đã đạt số lượng phù phép tối đa !");
				return false;
			}

			if (new Random().nextInt(100) < sc.getSuccessRate()) {

				mt.addEnchant(sc.getEnchant().getRootEnchantment(), 1, true);

				new_lores.addAll(lores);
				new_lores.add(2, "§r §7" + sc.getEnchant().getLore() + RomanNumerals.toNumerals(1));

				p.sendTitle("§a§lÉP/NÂNG CẤP THÀNH CÔNG", "§e§l" + sc.getEnchant().getLore());
				p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5, 1);
				p.getInventory().setItemInOffHand(null);

			} else {
				p.sendTitle("§c§lÉP/NÂNG CẤP THẤT BẠI", "§d§l" + sc.getEnchant().getLore());
				p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_BREAK, 1, 1);
				p.getInventory().setItemInOffHand(null);
				return false;
			}
		} else {
			if (mt.getEnchantLevel(sc.getEnchant().getRootEnchantment()) != sc.getLevel()) {
				p.sendMessage("§cCấp độ của phù phép trong vật phẩm (Cấp độ "
						+ RomanNumerals.toNumerals(mt.getEnchantLevel(sc.getEnchant().getRootEnchantment()))
						+ ") và cuộn giấy ma thuật (Cấp độ " + RomanNumerals.toNumerals(sc.getLevel())
						+ ") không đồng đều !");
				return false;
			}
			
			if (mt.getEnchantLevel(sc.getEnchant().getRootEnchantment()) == 10) {
				p.sendMessage("§cPhù phép " + sc.getEnchant().getLore() + "đã đạt cấp độ tối đa (Cấp độ X)");
				return false;
			}
			
			for (String s : lores) {
				if (s.replace('§', 'x').contains(string.replace('§', 'x'))) {

					if (new Random().nextInt(100) < sc.getSuccessRate()) {

						mt.removeEnchant(sc.getEnchant().getRootEnchantment());
						mt.addEnchant(sc.getEnchant().getRootEnchantment(), sc.getLevel() + 1, true);
						new_lores
								.add("§r §7" + sc.getEnchant().getLore() + RomanNumerals.toNumerals(sc.getLevel() + 1));
						p.sendTitle("§a§lÉP/NÂNG CẤP THÀNH CÔNG", "§e§l" + sc.getEnchant().getLore());
						p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5, 1);
						p.getInventory().setItemInOffHand(null);
					} else {
						p.sendTitle("§c§lÉP/NÂNG CẤP THẤT BẠI", "§d§l" + sc.getEnchant().getLore());
						p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_BREAK, 1, 1);
						p.getInventory().setItemInOffHand(null);
						return false;
					}
				} else {
					new_lores.add(s);
				}
			}
		}

		mt.setLore(new_lores);
		i.setItemMeta(mt);
		p.updateInventory();
		return true;
	}

}
