package com.wingx.pubmine.scroll;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;

public class ScrollEvents implements Listener {

	@EventHandler
	public void SCROLL_USE_CALL_EVENT(PlayerSwapHandItemsEvent e) {
	
		Player p = e.getPlayer();
		ItemStack main = p.getInventory().getItemInMainHand();
		ItemStack off = p.getInventory().getItemInOffHand();
		
		if(main == null || off == null) {
			return;
		}
		
		if(Scroll.isScroll(off) == false) {
			return;
		}
		
		e.setCancelled(true);
		Scroll.upgrade(p, main, off);
		
	}
	
}
