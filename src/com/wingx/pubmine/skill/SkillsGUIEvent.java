package com.wingx.pubmine.skill;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class SkillsGUIEvent implements Listener {

	public int LEVEL_TO_UNLOCK = 30;

	@EventHandler
	public void SKILL_GUI_UNLOCK_SKILL_CALL_EVENT(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		ItemStack i = e.getCurrentItem();

		if (i == null) {
			return;
		}

		if (!(e.getView().getTitle().equalsIgnoreCase(""))) {
			return;
		}

		if (i.hasItemMeta() == false || i.getItemMeta().hasDisplayName() == false) {
			return;
		}

		if (i.getType() != Material.BARRIER) {
			e.setCancelled(true);
			return;
		}

		if (e.getView().getTopInventory().contains(i)) {
			e.setCancelled(true);

			String SKILL_NAME = i.getItemMeta().getDisplayName().replaceFirst("§c", "");

			if (p.getLevel() >= 30) {
				p.setLevel(p.getLevel() - 30);

				File file = new File("plugins" + File.separator + "RPGTale" + File.separator + "players"
						+ File.separator + p.getName() + ".yml");
				FileConfiguration config = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(file);

				config.set("SKILLS." + SKILL_NAME, true);

				try {
					config.save(file);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				p.closeInventory();
				p.openInventory(SkillsGUI.getSkillsGUI(p));

				p.sendMessage("§aChúc mừng ! Bạn đã mở khóa kỹ năng §6§l" + SKILL_NAME);
				p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
				return;
			} else {
				e.setCancelled(true);
				p.playSound(p.getLocation(), Sound.ENTITY_BLAZE_BURN, 1, 1);
				p.sendMessage("§cBạn phải có đủ §c§l30 cấp độ §cđể mở khóa kỹ năng này !");
				return;
			}

		}

	}

	@EventHandler
	public void SKILL_GUI_SELECT_SKILL_CALL_EVENT(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		ItemStack i = e.getCurrentItem();

		if (i == null) {
			return;
		}

		if (!(e.getView().getTitle().equalsIgnoreCase(""))) {
			return;
		}

		if (i.hasItemMeta() == false || i.getItemMeta().hasDisplayName() == false) {
			return;
		}

		if (i.getType() != Material.BOOK) {
			e.setCancelled(true);
			return;
		}

		if (e.getView().getTopInventory().contains(i)) {
			e.setCancelled(true);

			String SKILL_NAME = i.getItemMeta().getDisplayName().replaceFirst("§e", "");

			File file = new File("plugins" + File.separator + "RPGTale" + File.separator + "players" + File.separator
					+ p.getName() + ".yml");
			FileConfiguration config = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(file);

			config.set("CHOOSE_SKILL", SKILL_NAME.toUpperCase());

			try {
				config.save(file);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			p.closeInventory();
			p.openInventory(SkillsGUI.getSkillsGUI(p));

			p.sendMessage("§aBạn đã chọn kỹ năng §6§l" + SKILL_NAME);
			p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
			return;
		}
	}

}
