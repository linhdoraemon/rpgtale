package com.wingx.pubmine.tale;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

import com.wingx.pubmine.boss.ArmorstandBoss;
import com.wingx.pubmine.boss.BossEvents;
import com.wingx.pubmine.boss.skill.BossSideSkills;
import com.wingx.pubmine.boss.skill.BossSkillType;
import com.wingx.pubmine.boss.skill.BossSkills;
import com.wingx.pubmine.data.PlayerRegisterDataEvent;
import com.wingx.pubmine.element.ElementEvents;
import com.wingx.pubmine.element.ElementType;
import com.wingx.pubmine.element.dust.DustGUI;
import com.wingx.pubmine.element.dust.DustGUIEvents;
import com.wingx.pubmine.element.dust.DustItem;
import com.wingx.pubmine.element.dust.ElementDust;
import com.wingx.pubmine.generator.ItemGenerator;
import com.wingx.pubmine.generator.ItemType;
import com.wingx.pubmine.mana.ManaActivate;
import com.wingx.pubmine.mana.ManaRegeneration;
import com.wingx.pubmine.potion.Effect;
import com.wingx.pubmine.potion.PotionEvents;
import com.wingx.pubmine.potion.type.BoostEXPPotion;
import com.wingx.pubmine.potion.type.GuardianAngelPotion;
import com.wingx.pubmine.potion.type.ImmortalPotion;
import com.wingx.pubmine.potion.type.StunPotion;
import com.wingx.pubmine.skill.SkillsGUIEvent;

public class RPGTale extends org.bukkit.plugin.java.JavaPlugin {
	public RPGTale() {
	}

	public void onEnable() {

		getLogger().info("Enabling plugin RPGTale .....");

		getLogger().info("Registering some initations ....");
		Effect.init();

		getLogger().info("Registering some call events .....");
		Bukkit.getPluginManager().registerEvents(new PotionEvents(), this);
		Bukkit.getPluginManager().registerEvents(new com.wingx.pubmine.skill.SkillsEvent(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerRegisterDataEvent(), this);
		Bukkit.getPluginManager().registerEvents(new ElementEvents(), this);
		Bukkit.getPluginManager().registerEvents(new DustGUIEvents(), this);
		Bukkit.getPluginManager().registerEvents(new ManaActivate(), this);
		Bukkit.getPluginManager().registerEvents(new SkillsGUIEvent(), this);
		Bukkit.getPluginManager().registerEvents(new BossEvents(), this);

		getLogger().info("Starting some tasks timer ....");
		new ManaRegeneration().runTaskTimer(this, 40, 40);

		getLogger().info("Initialization done. RPGTale enabled successfully !");
	}

	// DEV COMMANDS - not for release ver
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("testboss")) {
			ArmorStand s = p.getWorld().spawn(p.getLocation(), ArmorStand.class);
			ArmorstandBoss boss = new ArmorstandBoss(s);
			
			boss.setDisplayName("§a§lTest Boss");
			boss.setTotalHealth(100);
			
			boss.setItemInMainHand(new ItemStack(Material.DIAMOND_AXE), 15);
			boss.setItemInOffHand(new ItemStack(Material.SHIELD), 15);
			boss.setHelmet(new ItemStack(Material.WITHER_SKELETON_SKULL), 0);
			boss.setChestplate(new ItemStack(Material.GOLDEN_CHESTPLATE), 0);
			boss.setLeggings(new ItemStack(Material.GOLDEN_LEGGINGS), 0);
			boss.setBoots(new ItemStack(Material.GOLDEN_BOOTS), 0);

			BossSkills skill1 = new BossSkills(boss, BossSkillType.KNOCKUP, 3);
			skill1.setActivateMessage("§7[" + boss.getDisplayName() + "§7] §cActivated the first skill !");
			skill1.setSound(Sound.BLOCK_ANVIL_FALL);

			BossSkills skill2 = new BossSkills(boss, BossSkillType.POTION, 3);
			skill2.setPotionSkillType(PotionEffectType.POISON);
			skill1.setActivateMessage("§7[" + boss.getDisplayName() + "§7] §cActivated the second skill !");
			skill1.setSound(Sound.ENTITY_WITCH_CELEBRATE);
			BossSideSkills skill_2_side = new BossSideSkills(boss, BossSkillType.THUNDER, 6);
			skill_2_side.setSound(Sound.BLOCK_BAMBOO_BREAK);
			skill_2_side.setActivateMessage("§7[" + boss.getDisplayName() + "§7] §cActivated the second-side skill !");
			skill2.setSideSkills(skill_2_side);
			
			boss.setSkills(skill1, skill2);
			boss.setSkillRange(10);
			
			boss.updateHealthBar();
			
			boss.update();
			
			boss.startSkilling();
			
			p.sendMessage("§a§lSummon boss successfully !");
		}
		

		if (cmd.getName().equalsIgnoreCase("setname")) {
			if (args[0] == null) {
				p.sendMessage("§cSử dụng : /setname <tên mới>");
				return true;
			}
			ItemStack i = p.getInventory().getItemInMainHand();
			ItemMeta mt = i.getItemMeta();
			String name = "";
			for (String s : args) {
				name = name.concat(" " + s);
			}
			mt.setDisplayName(name.replaceFirst(" ", "").replace('&', '§'));
			i.setItemMeta(mt);
			p.updateInventory();
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("addlore")) {
			if (args[0] == null) {
				p.sendMessage("§cSử dụng : /addlore <dòng mới>");
				return true;
			}
			ItemStack i = p.getInventory().getItemInMainHand();
			ItemMeta mt = i.getItemMeta();
			String name = "";
			for (String s : args) {
				name = name.concat(" " + s);
			}
			if (mt.hasLore() == false) {
				mt.setLore(Arrays.asList(name.replace('&', '§')));
			} else {
				List<String> lores = mt.getLore();
				lores.add(name.replaceFirst(" ", "").replace('&', '§'));
				mt.setLore(lores);
			}
			i.setItemMeta(mt);
			p.updateInventory();
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("removelore")) {
			if (args[0] == null) {
				p.sendMessage("§cSử dụng : /removelore <dòng số>");
				return true;
			}
			ItemStack i = p.getInventory().getItemInMainHand();
			ItemMeta mt = i.getItemMeta();
			int d = Integer.parseInt(args[0]);
			if (mt.hasLore() == false) {
				p.sendMessage("§cVật phẩm này không có lores !");
				return true;
			} else {
				List<String> lores = mt.getLore();
				if (lores.remove(lores.get(d))) {
					mt.setLore(lores);
				} else {
					p.sendMessage("§cVật phẩm này không có dòng thứ " + d);
				}
			}
			i.setItemMeta(mt);
			p.updateInventory();
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("rpgtale")) {
			if (p.isOp() || p.hasPermission("*")) {
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("itemtype")) {
						for(ItemType type : ItemType.values()) {
							p.sendMessage("§a- " + type.toString());
						}
						return true;
					}
					
					if (args[0].equalsIgnoreCase("potions")) {
						p.getInventory().addItem(new StunPotion());
						p.getInventory().addItem(new BoostEXPPotion(Arrays.asList(BoostEXPPotion.NAMES).get(0)));
						p.getInventory().addItem(new GuardianAngelPotion(Arrays.asList(GuardianAngelPotion.NAMES).get(0)));
						p.getInventory().addItem(new ImmortalPotion(Arrays.asList(ImmortalPotion.NAMES).get(0)));
					}
					
					if (args[0].equalsIgnoreCase("sockets")) {
						
						if(args[1] == null) {
							p.sendMessage("§cVui lòng nhập cấp độ của bột nâng cấp (từ 1 - 5)");
							return true;
						}
						
						p.getInventory().addItem(DustItem.EARTH_POWDER(Integer.parseInt(args[1])));
						p.getInventory().addItem(DustItem.FIRE_POWDER(Integer.parseInt(args[1])));
						p.getInventory().addItem(DustItem.WIND_POWDER(Integer.parseInt(args[1])));
						p.getInventory().addItem(DustItem.WATER_POWDER(Integer.parseInt(args[1])));
						p.getInventory().addItem(DustItem.THUNDER_POWDER(Integer.parseInt(args[1])));
					}
					
					if (args[0].equalsIgnoreCase("addsocket")) {
						if(p.getInventory().getItemInMainHand() == null) {
							p.sendMessage("§cVui lòng cầm vật phẩm cần thêm ô chứa bột nâng cấp ở tay chính !");
							return true;
						}
						
						ElementDust.addDustSlot(p, p.getInventory().getItemInMainHand());
						p.sendMessage("§aThêm ô chứa bột nâng cấp thành công !");
					}
					
					
					
					if (args[0].equalsIgnoreCase("generator")) {
						
						if(args[1] == null || args[2] == null) {
							return true;
						}

						p.getInventory()
						.addItem(new ItemGenerator(ItemType.valueOf(args[1].toUpperCase()), Material.valueOf(args[2].toUpperCase()))
								.getRandomItem());
						
						return true;
					}
					
					if (args[0].equalsIgnoreCase("element")) {
						if (args[1] == null || args[2] == null || args[3] == null || args[4] == null) {
							p.sendMessage(
									"§cSử dụng : /rpgtale element <Tên nguyên tố> <Thông số nguyên tố min> <Thông số nguyên tố max> <true/false : true == Defense>");
							p.sendMessage("§cCác nguyên tố hiện có : FIRE, EARTH, WATER, WIND, THUNDER");
							return true;
						} else {
							ElementType type = ElementType.valueOf(args[1].toUpperCase());
							type.addElementLore(p, p.getInventory().getItemInMainHand(), Integer.parseInt(args[2]),
									Integer.parseInt(args[3]), Boolean.valueOf(args[4]));
							return true;
						}
					}
				} else {
					p.sendMessage(
							"§e/rpgtale element <Tên nguyên tố> <Thông số nguyên tố min> <Thông số nguyên tố max> <true/false : true == Defense>");
					p.sendMessage("§e/rpgtale generator <loại item (rpgtale itemtype)> <material>");
					p.sendMessage("§e/rpgtale potions");
					p.sendMessage("§e/rpgtale addsocket");
					p.sendMessage("§e/rpgtale sockets");
					p.sendMessage("§e/setname <Tên mới>");
					p.sendMessage("§e/addlore <Dòng mới>");
					p.sendMessage("§e/removelore <Dòng số>");
					return true;
				}
			} else {
				p.sendMessage("§cBạn không có quyền sử dụng lệnh này !");
				return true;
			}
		}

		if (cmd.getName().equalsIgnoreCase("nangcap")) {
			p.openInventory(DustGUI.getMainGUI(p));

			p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
			return true;
		}

		return true;
	}
}
